package lvt.project.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.Notification;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.ext.Selectable;
import org.zkoss.zul.impl.MessageboxDlg;

import lvt.project.model.DiaChi;
import lvt.project.model.JsonParser;
import lvt.project.model.KhachHang;
import lvt.project.model.KhachHangs;
import lvt.project.model.PhuongXa;
import lvt.project.model.QuanHuyen;
import lvt.project.model.TinhThanhPho;

public class KhachHangController extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;

	@Wire
	private Textbox keywordBox;
	@Wire
	private Textbox txtMaKH, txtTenKH, txtDiaChi, txtHinhAnh;
	@Wire
	private Listbox khachHangListbox;
	@Wire
	private Component detailBox;
	@Wire
	private Label lbMaKH, lbTenKH, lbDiaChi;
	@Wire
	private Image previewImage, imgUpload;
	@Wire
	private Button btnSave, btnUpload, btnAdd;
	@Wire
	private Label lbCartMaKH, lbCartTenKH, lbCartDiaChi;
	@Wire
	private Combobox cboGioiTinh, cboTinhThanh, cboQuanHuyen, cboPhuongXa;
	@Wire
	private Radio raURL, raFILE;
	@Wire
	private Radiogroup rbtnGroupFileAnh;
	// Data for view
	private KhachHangs khachHangs = new KhachHangs();
	public static ListModelList<KhachHang> KhanhHangLML;
	DiaChi dc = new DiaChi();

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		// get data from service and wrap it to list-model for the view
		KhanhHangLML = new ListModelList<KhachHang>(khachHangs.lstKH);
		khachHangListbox.setModel(KhanhHangLML);

		cboGioiTinh.setSelectedIndex(0);

		cboTinhThanh.getChildren().clear();
		Comboitem cbItem0 = new Comboitem("--Chọn Tỉnh/TP--");
		cbItem0.setDisabled(true);
		cbItem0.setValue("NoValue");
		cboTinhThanh.appendChild(cbItem0);
		
		for (TinhThanhPho ttp : DiaChi.lstThanhPho) {
			Comboitem cbItem = new Comboitem(ttp.getTenTTP());
			cbItem.setValue(ttp.getMaTTP());
			cboTinhThanh.appendChild(cbItem);
		}
		cboTinhThanh.setSelectedIndex(0);
		cboQuanHuyen.setSelectedIndex(0);
		cboPhuongXa.setSelectedIndex(0);
	}

	@Listen("onClick = #searchButton")
	public void search() {
		String keyword = keywordBox.getValue();
		ListModelList<KhachHang> KhanhHangLML_Search = new ListModelList<KhachHang>(searchKH(keyword));
		KhanhHangLML = KhanhHangLML_Search;
	}

	public List<KhachHang> searchKH(String keyword) {
		keyword = keyword.toLowerCase();
		List<KhachHang> lstKHSearch = new ArrayList<KhachHang>();
		for (KhachHang khachHang : KhanhHangLML) {
			if (khachHang.getMaKH().toLowerCase().contains(keyword) == true
					|| khachHang.getTenKH().toLowerCase().contains(keyword) == true
					|| khachHang.getDiaChi().toLowerCase().contains(keyword) == true) {
				lstKHSearch.add(khachHang);
			}
		}
		return lstKHSearch;
	}

	public void remove() {
		Execution execution = Executions.getCurrent();
		String maKH = execution.getParameter("id");
		Messagebox.show(maKH);
	}

	public void emptyText() {
		txtTenKH.setText("");
		txtHinhAnh.setText("");
		txtDiaChi.setText("");
	}

	@Listen("onClick=#btnClear")
	public void clear() {
		emptyText();
	}

	@Listen("onSelect = #khachHangListbox")
	public void showDetail() {
		detailBox.setVisible(true);

		Set<KhachHang> selection = ((Selectable<KhachHang>) khachHangListbox.getModel()).getSelection();
		if (selection != null && !selection.isEmpty()) {
			KhachHang selected = selection.iterator().next();
			previewImage.setSrc(selected.getHinhAnh());
			lbMaKH.setValue(selected.getMaKH());
			lbTenKH.setValue(selected.getTenKH());
			lbDiaChi.setValue(selected.getDiaChi());

//			lbCartMaKH.setValue(selected.getMaKH());
//			lbCartTenKH.setValue(selected.getTenKH());
//			lbCartDiaChi.setValue(selected.getDiaChi());

			if (selected.getGioiTinh().equals("Nam"))
				cboGioiTinh.setSelectedIndex(0);
			else
				cboGioiTinh.setSelectedIndex(1);

		}
	}

	// Cách 2:

	// when user clicks the delete button of each todo on the list
	@Listen("onDeleteKH = #khachHangListbox")
	public void DeleteKH(ForwardEvent evt) {
		Button btn = (Button) evt.getOrigin().getTarget();
		Listitem litem = (Listitem) btn.getParent().getParent();

		KhachHang kh = (KhachHang) litem.getValue();

//		//delete data
//		khachHangs.deleteKH(kh);

		// update the model of listbox
		KhanhHangLML.remove(kh);
	}

	@Listen("onEditKH = #khachHangListbox")
	public void openModelEditKH(ForwardEvent evt) {
		Button btn = (Button) evt.getOrigin().getTarget();
		Listitem litem = (Listitem) btn.getParent().getParent();

		
		KhachHangs.khachHangSelect = (KhachHang) litem.getValue();

		// create a window programmatically and use it as a modal dialog.
		Window window = (Window) Executions.createComponents("/KhachHang_EditDialog.zul", null, null);
		window.doModal();
	}

	@Listen("onClick=#btnAdd")
	public void addKH() {
		if (txtMaKH.getText().equals("")) {
			Notification.show("Mã khách hàng không được để trống.", "error", txtMaKH, "end_after", 2000, false);
			txtMaKH.focus();
			return;
		}
		
		for (KhachHang kh : KhanhHangLML) {
			if (kh.getMaKH().equals(txtMaKH.getText())) {
				Notification.show("Mã khách hàng đã tồn tại.", "error", txtMaKH, "end_after", 3000, false);
				return;
			}
		}
		if (txtTenKH.getText().equals("")) {
			Notification.show("Tên khách hàng không được để trống.", "error", txtTenKH, "end_after", 2000, false);
			txtTenKH.focus();
			return;
		}
		if (cboGioiTinh.getSelectedIndex() < 0) {
			Notification.show("Vui lòng chọn giới tính.", "error", cboGioiTinh, "end_after", 3000, false);
			cboGioiTinh.focus();
			return;
		}
		if (cboTinhThanh.getSelectedIndex()<=0) {
			Notification.show("Vui lòng chọn Tỉnh/TP.", "error", cboTinhThanh, "end_after", 2000, false);
			cboTinhThanh.focus();
			return;
		}
		if (cboQuanHuyen.getSelectedIndex()<=0) {
			Notification.show("Vui lòng chọn Quận/Huyện.", "error", cboQuanHuyen, "end_after", 2000, false);
			cboQuanHuyen.focus();
			return;
		}
		if (cboPhuongXa.getSelectedIndex()<=0) {
			Notification.show("Vui lòng chọn Phường/Xã.", "error", cboPhuongXa, "end_after", 2000, false);
			cboPhuongXa.focus();
			return;
		}
		if (txtDiaChi.getText().equals("")) {
			Notification.show("Số nhà, tên đường không được để trống.", "error", txtDiaChi, "end_after", 1000, false);
			txtDiaChi.focus();
			return;
		}
		String diaChi = txtDiaChi.getText().replace(",","") + ", " 
		+ cboPhuongXa.getSelectedItem().getLabel() +", "
				+ cboQuanHuyen.getSelectedItem().getLabel() +", "
				+ cboTinhThanh.getSelectedItem().getLabel();
		KhachHang kh = new KhachHang();
		kh.setMaKH(autoCreateIDKH());
		kh.setTenKH(txtTenKH.getText());
		kh.setDiaChi(diaChi);

		if (rbtnGroupFileAnh.getSelectedIndex() == 0)
			kh.setHinhAnh(txtHinhAnh.getText());
		else
			kh.setHinhAnh(imgUpload.getSrc());
		kh.setGioiTinh(cboGioiTinh.getSelectedItem().getValue());

		KhanhHangLML.add(kh);
		khachHangs.lstKH.add(kh);
		// set the new selection
		KhanhHangLML.addToSelection(kh);
		imgUpload.setSrc("");
		Notification.show("Thêm mới thành công.", "info", btnAdd, "end_after", 1000, false);
	}

	@Listen("onCheck = #rbtnGroupFileAnh")
	public void changeFile() {
		if (rbtnGroupFileAnh.getSelectedIndex()==0) {
			txtHinhAnh.setDisabled(false);
			btnUpload.setDisabled(true);
		} else {
			txtHinhAnh.setDisabled(true);
			btnUpload.setDisabled(false);
		}
	}

	@Listen("onSelect = #cboTinhThanh")
	public void loadCboQuanhuyen() {
		cboQuanHuyen.getChildren().clear();
		cboPhuongXa.getChildren().clear();
		Comboitem cbItem0 = new Comboitem("--Quận/Huyện--");
		cbItem0.setDisabled(true);
		cbItem0.setValue("NoValueQH");
		cboQuanHuyen.appendChild(cbItem0);
		if (cboTinhThanh.getSelectedIndex() > 0) {
			for (TinhThanhPho ttp : DiaChi.lstThanhPho) {
				if (ttp.getMaTTP().equals(cboTinhThanh.getSelectedItem().getValue().toString())) {
					DiaChi.tinhThanhPhoSelection = ttp;
					for (QuanHuyen qh : ttp.getLstQuanHuyen()) {
						Comboitem cbItem = new Comboitem(qh.getTenQH());
						cbItem.setValue(qh.getMaQH());
						cboQuanHuyen.appendChild(cbItem);
					}
					break;
				}
			}
		}
		cboQuanHuyen.setSelectedIndex(0);
	}

	@Listen("onSelect = #cboQuanHuyen")
	public void loadCboPhuongXa() {
		cboPhuongXa.getChildren().clear();
		
		Comboitem cbItem0 = new Comboitem("--Phường/Xã--");
		cbItem0.setDisabled(true);
		cbItem0.setValue("NoValuePX");
		cboPhuongXa.appendChild(cbItem0);
		
		if (cboQuanHuyen.getSelectedIndex() > 0) {
			for (QuanHuyen qh : DiaChi.tinhThanhPhoSelection.getLstQuanHuyen()) {
				if (qh.getMaQH().equals(cboQuanHuyen.getSelectedItem().getValue().toString())) {
					for (PhuongXa px : qh.getLstPhuongXa()) {
						Comboitem cbItem = new Comboitem(px.getTenPX());
						cbItem.setValue(px.getMaPX());
						cboPhuongXa.appendChild(cbItem);
					}
					break;
				}
			}
			
		}
		cboPhuongXa.setSelectedIndex(0);
	}
	
	public String autoCreateIDKH()
	{
		KhachHang kh = KhanhHangLML.get(KhanhHangLML.size()-1);
		String id = kh.getMaKH().substring(2);
		int intId = Integer.parseInt(id)+1;
		
		
		return "KH"+intId;
	}

}
