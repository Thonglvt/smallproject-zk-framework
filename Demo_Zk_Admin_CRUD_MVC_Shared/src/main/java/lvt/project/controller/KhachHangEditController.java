package lvt.project.controller;

import java.util.Iterator;

import org.w3c.dom.Text;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.Notification;
import org.zkoss.zul.*;

import lvt.project.model.DiaChi;
import lvt.project.model.KhachHang;
import lvt.project.model.KhachHangs;
import lvt.project.model.PhuongXa;
import lvt.project.model.QuanHuyen;
import lvt.project.model.TinhThanhPho;

public class KhachHangEditController extends SelectorComposer<Component> {

	@Wire
	Window modalDialog;
	@Wire
	Textbox txtMaKH, txtTenKH, txtDiaChi;
	@Wire
	private Combobox cboGioiTinh_edit, cboTinhThanh, cboQuanHuyen, cboPhuongXa;
	@Wire
	Button btnSave;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		modalDialog.setTitle("Edit data #" + KhachHangs.khachHangSelect.getMaKH() + ":");
		// Get select data from KhachHangController take update
		txtMaKH.setValue("#" + KhachHangs.khachHangSelect.getMaKH());
		txtTenKH.setValue(KhachHangs.khachHangSelect.getTenKH());
		cboGioiTinh_edit.setValue(KhachHangs.khachHangSelect.getGioiTinh());
		txtTenKH.focus();

		cboTinhThanh.getChildren().clear();
		Comboitem cbItem0 = new Comboitem("--Chọn Tỉnh/TP--");
		cbItem0.setDisabled(true);
		cbItem0.setValue("NoValue");
		cboTinhThanh.appendChild(cbItem0);

		for (TinhThanhPho ttp : DiaChi.lstThanhPho) {
			Comboitem cbItem = new Comboitem(ttp.getTenTTP());
			cbItem.setValue(ttp.getMaTTP());
			cboTinhThanh.appendChild(cbItem);

		}

		String[] strSplitAddress = KhachHangs.khachHangSelect.getDiaChi().split(",");

		int dem = 0;
		String tp = strSplitAddress[3];
		String qh = strSplitAddress[2];
		String px = strSplitAddress[1];
		String soNha = strSplitAddress[0];

		txtDiaChi.setValue(soNha);

		int indexTP = 0;
		TinhThanhPho ttpFind = new TinhThanhPho();
		for (TinhThanhPho item : DiaChi.lstThanhPho) {
			indexTP++;
			if (item.getTenTTP().trim().toLowerCase().equals(tp.trim().toLowerCase())) {
				ttpFind = item;
				break;
			}
		}
		int indexQH = 0;
		QuanHuyen qhFind = new QuanHuyen();
		for (QuanHuyen item : ttpFind.getLstQuanHuyen()) {
			indexQH++;
			if (item.getTenQH().trim().toLowerCase().equals(qh.trim().toLowerCase())) {
				qhFind = item;
				break;
			}
		}
		int indexPX = 0;
		PhuongXa pxFind = new PhuongXa();
		for (PhuongXa item : qhFind.getLstPhuongXa()) {
			indexPX++;
			if (item.getTenPX().trim().toLowerCase().equals(px.trim().toLowerCase())) {
				pxFind = item;
				break;
			}
		}

		cboQuanHuyen.getChildren().clear();
		cboPhuongXa.getChildren().clear();
		Comboitem cbItem1 = new Comboitem("--Quận/Huyện--");
		cbItem1.setDisabled(true);
		cbItem1.setValue("NoValueQH");
		cboQuanHuyen.appendChild(cbItem1);
		for (QuanHuyen item : ttpFind.getLstQuanHuyen()) {
			Comboitem cbItem = new Comboitem(item.getTenQH());
			cbItem.setValue(item.getMaQH());
			cboQuanHuyen.appendChild(cbItem);
		}

		cboPhuongXa.getChildren().clear();
		Comboitem cbItem2 = new Comboitem("--Phường/Xã--");
		cbItem2.setDisabled(true);
		cbItem2.setValue("NoValuePX");
		cboPhuongXa.appendChild(cbItem2);
		for (PhuongXa item : qhFind.getLstPhuongXa()) {
			Comboitem cbItem = new Comboitem(item.getTenPX());
			cbItem.setValue(item.getMaPX());
			cboPhuongXa.appendChild(cbItem);
		}
		
		cboTinhThanh.setSelectedIndex(indexTP);
		cboQuanHuyen.setSelectedIndex(indexQH);
		cboPhuongXa.setSelectedIndex(indexPX);
	}

	@Listen("onClick = #btnSave")
	public void saveKH() {
		if (cboGioiTinh_edit.getSelectedIndex() < 0) {
			Notification.show("Vui lòng chọn giới tính.", "error", cboGioiTinh_edit, "end_after", 1000, false);
			cboGioiTinh_edit.focus();
			return;
		}
		if (txtTenKH.getText().equals("")) {
			Notification.show("Tên khách hàng không được để trống.", "error", txtTenKH, "end_after", 1000, false);
			txtTenKH.focus();
			return;
		}
		if (cboTinhThanh.getSelectedIndex()<=0) {
			Notification.show("Vui lòng chọn Tỉnh/TP.", "error", cboTinhThanh, "end_after", 2000, false);
			cboTinhThanh.focus();
			return;
		}
		if (cboQuanHuyen.getSelectedIndex()<=0) {
			Notification.show("Vui lòng chọn Quận/Huyện.", "error", cboQuanHuyen, "end_after", 2000, false);
			cboQuanHuyen.focus();
			return;
		}
		if (cboPhuongXa.getSelectedIndex()<=0) {
			Notification.show("Vui lòng chọn Phường/Xã.", "error", cboPhuongXa, "end_after", 2000, false);
			cboPhuongXa.focus();
			return;
		}
		if (txtDiaChi.getText().equals("")) {
			Notification.show("Số nhà, tên đường không được để trống.", "error", txtDiaChi, "end_after", 1000, false);
			txtDiaChi.focus();
			return;
		}
		for (KhachHang kh : KhachHangController.KhanhHangLML) {
			if (kh.getMaKH().equals(KhachHangs.khachHangSelect.getMaKH())) {
				KhachHang khNew = new KhachHang();
				khNew.setMaKH(KhachHangs.khachHangSelect.getMaKH());
				khNew.setTenKH(txtTenKH.getText());
				String diaChi = txtDiaChi.getText().replace(",", "") + ", " + cboPhuongXa.getSelectedItem().getLabel()
						+ ", " + cboQuanHuyen.getSelectedItem().getLabel() + ", "
						+ cboTinhThanh.getSelectedItem().getLabel();
				khNew.setDiaChi(diaChi);
				khNew.setGioiTinh(cboGioiTinh_edit.getSelectedItem().getValue());
				khNew.setHinhAnh(KhachHangs.khachHangSelect.getHinhAnh());
				KhachHangController.KhanhHangLML.set(KhachHangController.KhanhHangLML.indexOf(kh), khNew);
				KhachHangController.KhanhHangLML.addSelection(khNew);
				Notification.show("Cập nhật thành công.", "info", modalDialog, "after_end", 3000, false);
				break;

			}
		}

		modalDialog.detach();
	}

	@Listen("onSelect = #cboTinhThanh")
	public void loadCboQuanhuyen() {
		cboQuanHuyen.getChildren().clear();
		cboPhuongXa.getChildren().clear();
		Comboitem cbItem0 = new Comboitem("--Quận/Huyện--");
		cbItem0.setDisabled(true);
		cbItem0.setValue("NoValueQH");
		cboQuanHuyen.appendChild(cbItem0);
		if (cboTinhThanh.getSelectedIndex() > 0) {
			for (TinhThanhPho ttp : DiaChi.lstThanhPho) {
				if (ttp.getMaTTP().equals(cboTinhThanh.getSelectedItem().getValue().toString())) {
					DiaChi.tinhThanhPhoSelectionEdit = ttp;
					for (QuanHuyen qh : ttp.getLstQuanHuyen()) {
						Comboitem cbItem = new Comboitem(qh.getTenQH());
						cbItem.setValue(qh.getMaQH());
						cboQuanHuyen.appendChild(cbItem);
					}
					break;
				}
			}
		}
		cboQuanHuyen.setSelectedIndex(0);
	}

	@Listen("onSelect = #cboQuanHuyen")
	public void loadCboPhuongXa() {
		cboPhuongXa.getChildren().clear();
		Comboitem cbItem0 = new Comboitem("--Phường/Xã--");
		cbItem0.setDisabled(true);
		cbItem0.setValue("NoValuePX");
		cboPhuongXa.appendChild(cbItem0);

		if (cboQuanHuyen.getSelectedIndex() > 0) {
			for (QuanHuyen qh : DiaChi.tinhThanhPhoSelectionEdit.getLstQuanHuyen()) {
				if (qh.getMaQH().equals(cboQuanHuyen.getSelectedItem().getValue().toString())) {
					for (PhuongXa px : qh.getLstPhuongXa()) {
						Comboitem cbItem = new Comboitem(px.getTenPX());
						cbItem.setValue(px.getMaPX());
						cboPhuongXa.appendChild(cbItem);
					}
					break;
				}
			}

		}
		cboPhuongXa.setSelectedIndex(0);
	}
}
