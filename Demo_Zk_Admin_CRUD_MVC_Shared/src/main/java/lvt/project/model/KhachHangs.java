package lvt.project.model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.zhtml.Messagebox;
import org.zkoss.zhtml.Var;

public class KhachHangs {

	public List<KhachHang> lstKH = new ArrayList<KhachHang>();

	public static KhachHang khachHangSelect = new KhachHang();
	
	public void getAll() {
		this.lstKH.add(new KhachHang("KH001", "Lê Văn Thông", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://zpsocial-f49-org.zadn.vn/547240747cda9084c9cb.jpg","Nam"));
		this.lstKH.add(new KhachHang("KH002", "Nguyễn Thiện Quang", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://zpsocial-f48-org.zadn.vn/af5e80e21d5ff101a84e.jpg","Nam"));
		this.lstKH.add(new KhachHang("KH003", "Nguyễn Nhựt Nam", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://zpsocial-f48-org.zadn.vn/e73470b1dcb333ed6aa2.jpg","Nam"));
		this.lstKH.add(new KhachHang("KH004", "Lâm Chí Khang", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://zpsocial-f43-org.zadn.vn/55ef7dfde93b05655c2a.jpg","Nam"));
		this.lstKH.add(new KhachHang("KH005", "No name 1", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://zpsocial-f43-org.zadn.vn/55ef7dfde93b05655c2a.jpg","Nam"));
		this.lstKH.add(new KhachHang("KH006", "No name 2", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://cdn.searchenginejournal.com/wp-content/uploads/2022/04/reverse-image-search-627b7e49986b0-sej-760x400.png","Nam"));
		this.lstKH.add(new KhachHang("KH007", "No name 3", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg","Nữ"));
		this.lstKH.add(new KhachHang("KH008", "No name 4", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://znews-stc.zdn.vn/static/topic/person/noo.jpg","Nữ"));
		this.lstKH.add(new KhachHang("KH009", "No name 5", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://vnn-imgs-f.vgcloud.vn/2020/11/25/08/bui-anh-tuan-cover-love-song-tang-sinh-nhat-thay-ho-ngoc-ha.jpg","Nữ"));
		this.lstKH.add(new KhachHang("KH010", "No name 6", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://znews-stc.zdn.vn/static/topic/person/hqhieu.jpg","Nữ"));
		this.lstKH.add(new KhachHang("KH011", "No name 7", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://avatar-ex-swe.nixcdn.com/singer/avatar/2018/06/27/e/8/8/5/1530074198530_600.jpg","Nữ"));
		this.lstKH.add(new KhachHang("KH012", "No name 8", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://nld.mediacdn.vn/2020/9/1/huong-giang-4-1598920415099274908299.jpg","Nữ"));
		this.lstKH.add(new KhachHang("KH013", "No name 9", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://suckhoedoisong.qltns.mediacdn.vn/324455921873985536/2022/3/24/karik-5-16480791817311887236776.jpg","Nữ"));
		this.lstKH.add(new KhachHang("KH014", "No name 10", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://i.vietgiaitri.com/2022/4/2/trieu-lo-tu-gay-soc-khi-dang-bai-to-tinh-tieu-chien-ekip-thanh-tra-xanh-noi-gi-5f4-6384680.jpg","Nữ"));
		this.lstKH.add(new KhachHang("KH015", "No name 11", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://avatar-ex-swe.nixcdn.com/singer/avatar/2019/12/16/b/e/6/c/1576471397360_600.jpg","Nữ"));
		this.lstKH.add(new KhachHang("KH016", "No name 12", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://nld.mediacdn.vn/2021/1/5/anh-chup-man-hinh-2021-01-05-luc-123029-ch-1609824658692305050059.png","Nữ"));
		this.lstKH.add(new KhachHang("KH017", "No name 13", "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi",
				"https://ss-images.saostar.vn/wp700/pc/1654708780374/saostar-sdsr8gxykqodoy7p.jpg","Nữ"));
	}

	public KhachHangs(List<KhachHang> lstKH) {
		this.lstKH = lstKH;
	}

	public KhachHangs() {
		getAll();
	}

	public List<KhachHang> search(String key) {
		key = key.toLowerCase();
		List<KhachHang> lstKHSearch = new ArrayList<KhachHang>();
		for (KhachHang khachHang : lstKH) {
			if (khachHang.getMaKH().toLowerCase().contains(key) == true
					|| khachHang.getTenKH().toLowerCase().contains(key) == true
					|| khachHang.getDiaChi().toLowerCase().contains(key) == true
					||khachHang.getGioiTinh().toLowerCase().contains(key) == true) {
				lstKHSearch.add(khachHang);
			}
		}
		return lstKHSearch;
	}

	public void deleteKH(KhachHang kh) {
		this.lstKH.remove(kh);
	}

}
