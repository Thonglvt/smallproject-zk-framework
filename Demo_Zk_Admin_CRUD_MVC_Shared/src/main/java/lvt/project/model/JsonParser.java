package lvt.project.model;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

public class JsonParser {
	
	public static String getJSONFromFile(String filename) {
		String jsonText = "";
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(filename,Charset.forName("UTF-8")));

			String line;
			while ((line = bufferedReader.readLine()) != null) {
//				jsonText += line + "\n";
				sb.append(line + "\n");
			}

			bufferedReader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	public static String getJSONFromURL(String strUrl){
		String jsonText = "";
		StringBuilder sb = new StringBuilder();
		try {
			URL url = new URL(strUrl);
			InputStream is = url.openStream();

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(is, Charset.forName("UTF-8")));

			String line;
			while ((line = bufferedReader.readLine()) != null) {
				sb.append(line + "\n");
//				jsonText += line + "\n";
			}

			is.close();
			bufferedReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
}
