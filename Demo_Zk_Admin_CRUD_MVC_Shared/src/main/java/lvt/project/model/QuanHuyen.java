package lvt.project.model;

import java.util.ArrayList;
import java.util.List;

public class QuanHuyen {
	private String maQH,tenQH;
	private List<PhuongXa> lstPhuongXa = new ArrayList<PhuongXa>();
	public String getMaQH() {
		return maQH;
	}
	public void setMaQH(String maQH) {
		this.maQH = maQH;
	}
	public String getTenQH() {
		return tenQH;
	}
	public void setTenQH(String tenQH) {
		this.tenQH = tenQH;
	}
	public List<PhuongXa> getLstPhuongXa() {
		return lstPhuongXa;
	}
	public void setLstPhuongXa(List<PhuongXa> lstPhuongXa) {
		this.lstPhuongXa = lstPhuongXa;
	}
	public QuanHuyen(String maQH, String tenQH, List<PhuongXa> lstPhuongXa) {
		super();
		this.maQH = maQH;
		this.tenQH = tenQH;
		this.lstPhuongXa = lstPhuongXa;
	}
	public QuanHuyen() {
		super();
	}
	
	
	
}
