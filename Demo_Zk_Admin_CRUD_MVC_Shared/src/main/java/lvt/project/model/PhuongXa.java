package lvt.project.model;

public class PhuongXa {
	private String maPX, tenPX;

	public String getMaPX() {
		return maPX;
	}

	public void setMaPX(String maPX) {
		this.maPX = maPX;
	}

	public String getTenPX() {
		return tenPX;
	}

	public void setTenPX(String tenPX) {
		this.tenPX = tenPX;
	}

	public PhuongXa(String maPX, String tenPX) {
		super();
		this.maPX = maPX;
		this.tenPX = tenPX;
	}

	public PhuongXa() {
		super();
	}

}
