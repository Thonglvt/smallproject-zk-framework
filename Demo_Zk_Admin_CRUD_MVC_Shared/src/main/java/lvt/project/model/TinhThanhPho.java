package lvt.project.model;

import java.util.ArrayList;
import java.util.List;

public class TinhThanhPho {
	private String maTTP,tenTTP;
	private List<QuanHuyen> lstQuanHuyen = new ArrayList<QuanHuyen>();
	public String getMaTTP() {
		return maTTP;
	}
	public void setMaTTP(String maTTP) {
		this.maTTP = maTTP;
	}
	public String getTenTTP() {
		return tenTTP;
	}
	public void setTenTTP(String tenTTP) {
		this.tenTTP = tenTTP;
	}
	public List<QuanHuyen> getLstQuanHuyen() {
		return lstQuanHuyen;
	}
	public void setLstQuanHuyen(List<QuanHuyen> lstQuanHuyen) {
		this.lstQuanHuyen = lstQuanHuyen;
	}
	public TinhThanhPho(String maTTP, String tenTTP, List<QuanHuyen> lstQuanHuyen) {
		super();
		this.maTTP = maTTP;
		this.tenTTP = tenTTP;
		this.lstQuanHuyen = lstQuanHuyen;
	}
	public TinhThanhPho() {
		super();
	}
	
	
	
	
}
