package lvt.project.model;

public class KhachHang {

	private String maKH, tenKH, diaChi, hinhAnh, gioiTinh;

	public String getGioiTinh() {
		return gioiTinh;
	}

	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}

	public String getHinhAnh() {
		return hinhAnh;
	}

	public void setHinhAnh(String hinhAnh) {
		this.hinhAnh = hinhAnh;
	}

	public String getMaKH() {
		return maKH;
	}

	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}

	public String getTenKH() {
		return tenKH;
	}

	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public KhachHang(String maKH, String tenKh, String diaChi, String hinhAnh) {
		this.maKH = maKH;
		this.tenKH = tenKh;
		this.diaChi = diaChi;
		this.hinhAnh = hinhAnh;
	}

	public KhachHang(String maKH, String tenKh, String diaChi, String hinhAnh, String gioiTinh) {
		this.maKH = maKH;
		this.tenKH = tenKh;
		this.diaChi = diaChi;
		this.hinhAnh = hinhAnh;
		this.gioiTinh = gioiTinh;
	}

	public KhachHang() {
		this.maKH = "";
		this.tenKH = "";
		this.diaChi = "419 Phạm Xuân Hòa, Xã Phổ Cường, Thị xã Đức Phổ, Tỉnh Quảng Ngãi";
		this.hinhAnh = "https://zpsocial-f49-org.zadn.vn/547240747cda9084c9cb.jpg";
		this.gioiTinh = "Nam";
	}

}
