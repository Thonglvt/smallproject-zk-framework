package lvt.project.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.zkoss.zul.Messagebox;

public class DiaChi {

	public static List<TinhThanhPho> lstThanhPho = new ArrayList<TinhThanhPho>();
	public static List<String> lstStrThanhPho = new ArrayList<String>();
	public static TinhThanhPho tinhThanhPhoSelection = new TinhThanhPho();
	public static TinhThanhPho tinhThanhPhoSelectionEdit = new TinhThanhPho();
	
	public void init()
	{
		 String strJson = JsonParser.getJSONFromURL(
				 "https://provinces.open-api.vn/api/?depth=3"
				 );	
		 try {
			  /*************** Thanh pho ****************/
	            JSONArray jsonArrayMain = new JSONArray(strJson);
	            
	            for (int i = 0; i < jsonArrayMain.length(); i++) {
	                JSONObject jsonObjThanhPho = (JSONObject) jsonArrayMain.get(i);
	                
	                //Thêm tỉnh
	                TinhThanhPho ttp = new TinhThanhPho();
	                ttp.setTenTTP((String) jsonObjThanhPho.get("name"));
	                ttp.setMaTTP(jsonObjThanhPho.get("code").toString());
	                
	                //Set list Quận huyện của tỉnh
	                JSONArray jsonArrayQuanHuyen = (JSONArray) jsonObjThanhPho.get("districts");
	                for (int j = 0; j < jsonArrayQuanHuyen.length(); j++) {
	                	JSONObject jsonObjQuanHuyen = (JSONObject) jsonArrayQuanHuyen.get(j);
	                	QuanHuyen qh = new QuanHuyen();
	                	qh.setTenQH((String) jsonObjQuanHuyen.get("name"));
	                	qh.setMaQH( jsonObjQuanHuyen.get("code").toString());
	                	
	                	//Set list Phường xã của Quận huyện
		                JSONArray jsonArrayPhuongXa = (JSONArray) jsonObjQuanHuyen.get("wards");
		                for (int k = 0; k < jsonArrayPhuongXa.length(); k++) {
		                	JSONObject jsonObjPhuongXa = (JSONObject) jsonArrayPhuongXa.get(k);
		                	PhuongXa px = new PhuongXa();
		                	px.setMaPX( jsonObjPhuongXa.get("code").toString());
		                	px.setTenPX((String)jsonObjPhuongXa.get("name").toString());
		                	
		                	qh.getLstPhuongXa().add(px);
		                }
	                	ttp.getLstQuanHuyen().add(qh);
	                }
	                lstThanhPho.add(ttp);
	            }
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		 
	}
	
	public DiaChi()
	{
		init();
	}

	public static List<TinhThanhPho> getLstThanhPho() {
		return lstThanhPho;
	}

	public static void setLstThanhPho(List<TinhThanhPho> lstThanhPho) {
		DiaChi.lstThanhPho = lstThanhPho;
	}

	public static List<String> getLstStrThanhPho() {
		return lstStrThanhPho;
	}

	public static void setLstStrThanhPho(List<String> lstStrThanhPho) {
		DiaChi.lstStrThanhPho = lstStrThanhPho;
	}
	
}
